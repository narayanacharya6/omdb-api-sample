package com.narayanacharya.cubetask.di;

import android.app.Application;
import android.support.annotation.NonNull;

import com.narayanacharya.cubetask.BuildConfig;
import com.narayanacharya.cubetask.data.SearchRepositoryImpl;
import com.narayanacharya.cubetask.data.SearchRepository;
import com.narayanacharya.cubetask.data.remote.SearchService;
import com.narayanacharya.cubetask.util.network.RxErrorHandlingCallAdapterFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class AppModule {

    Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Application provideApplication() {
        return application;
    }

    @Provides
    @Singleton
    SearchRepository provideSearchRepository(@NonNull SearchService searchService) {
        return new SearchRepositoryImpl(searchService);
    }

    @Provides
    @Singleton
    SearchService provideSearchService(@NonNull Retrofit retrofit) {
        return retrofit.create(SearchService.class);
    }

    @Provides
    @Singleton
    Retrofit provideBaseRetrofit(@NonNull Retrofit.Builder retrofitBuilder) {
        return retrofitBuilder
                .baseUrl(BuildConfig.BASE_URL)
                .build();
    }

    @Provides
    @Singleton
    Retrofit.Builder provideRetrofitBuilder(@NonNull OkHttpClient okHttpClient,
                                            @NonNull GsonConverterFactory gsonConverterFactory,
                                            @NonNull RxErrorHandlingCallAdapterFactory rxErrorHandlingCallAdapterFactory) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(rxErrorHandlingCallAdapterFactory);
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addInterceptor(httpLoggingInterceptor);
        return builder.build();
    }

    @Provides
    @Singleton
    GsonConverterFactory provideGsonConverterFactory() {
        return GsonConverterFactory.create();
    }

    @Provides
    @Singleton
    RxErrorHandlingCallAdapterFactory provideRxErrorHandlingCallAdapterFactory() {
        return (RxErrorHandlingCallAdapterFactory) RxErrorHandlingCallAdapterFactory.create();
    }
}

