package com.narayanacharya.cubetask.data;

import com.narayanacharya.cubetask.data.remote.SearchService;
import com.narayanacharya.cubetask.data.remote.models.SearchResult;
import com.narayanacharya.cubetask.data.remote.models.SearchResults;

import rx.Observable;

/**
 * Created by Narayan on 23/03/17.
 */

public class SearchRepositoryImpl implements SearchRepository {

    private SearchService searchService;

    public SearchRepositoryImpl(SearchService searchService) {
        this.searchService = searchService;
    }

    @Override
    public Observable<SearchResults> getSearchResults(String query, String category) {
        return searchService.getSearchResults(query, category);
    }

    @Override
    public Observable<SearchResult> getSearchResult(String imdbId) {
        return searchService.getSearchResult(imdbId);
    }
}
