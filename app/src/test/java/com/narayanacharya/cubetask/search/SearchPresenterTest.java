package com.narayanacharya.cubetask.search;

import com.narayanacharya.cubetask.RxSchedulersOverrideRule;
import com.narayanacharya.cubetask.data.SearchRepository;
import com.narayanacharya.cubetask.data.remote.models.SearchResults;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import rx.Observable;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SearchPresenterTest {

    @Rule
    public final RxSchedulersOverrideRule mOverrideRule = new RxSchedulersOverrideRule();
    @Mock
    private SearchRepository searchRepository;
    @Mock
    private SearchContract.MovieSearchView movieSearchView;
    private SearchPresenter searchPresenter;

    @Before
    public void setup() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        // Get a reference to the class under test
        searchPresenter = new SearchPresenter(movieSearchView, searchRepository);
    }

    @Test
    public void failedMovieSearchResult() {
        // Prepare
        SearchResults searchResults = new SearchResults();
        searchResults.setResponse("False");
        when(searchRepository.getSearchResults(anyString(), anyString()))
                .thenReturn(Observable.just(searchResults));

        // When
        searchPresenter.search("wow", "movies");

        // Then
        verify(movieSearchView).onSearchFailed(searchResults);
    }

    @Test
    public void successfulMovieSearchResult() {
        // Prepare
        SearchResults searchResults = new SearchResults();
        searchResults.setResponse("True");
        when(searchRepository.getSearchResults(anyString(), anyString()))
                .thenReturn(Observable.just(searchResults));

        // When
        searchPresenter.search("wow", "movies");

        // Then
        verify(movieSearchView).onSearchSuccess(searchResults);
    }

    @Test
    public void emptyQuery() {
        // Prepare
        SearchResults searchResults = new SearchResults();
        searchResults.setResponse("True");
        when(searchRepository.getSearchResults(anyString(), anyString()))
                .thenReturn(Observable.just(searchResults));

        // When
        searchPresenter.search("", "movies");

        // Then
        verify(movieSearchView).onEmptySearchQuery();
    }

    @Test
    public void malformedResponseInSearchResult() {
        // Prepare
        SearchResults searchResults = new SearchResults();
        searchResults.setResponse("Tre");
        when(searchRepository.getSearchResults(anyString(), anyString()))
                .thenReturn(Observable.just(searchResults));

        // When
        searchPresenter.search("damn", "movies");

        // Then
        verify(movieSearchView).onUnknownErrorOccured();
    }
}
