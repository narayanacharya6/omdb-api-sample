package com.narayanacharya.cubetask.search;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.narayanacharya.cubetask.R;
import com.narayanacharya.cubetask.common.base.widgets.BaseTextView;
import com.narayanacharya.cubetask.data.remote.models.SearchResult;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

class SearchResultsAdapter extends RecyclerView.Adapter<SearchResultsAdapter.SearchResultsViewHolder> {

    private List<SearchResult> searchResults;
    private SearchResultsClickListener commander;

    interface SearchResultsClickListener {
        void onSearchResultClicked(SearchResult searchResult);
    }

    SearchResultsAdapter(List<SearchResult> searchResults, SearchResultsClickListener searchResultsClickListener) {
        this.searchResults = searchResults;
        this.commander = searchResultsClickListener;
    }

    void addSearchResult(SearchResult searchResult) {
        searchResults.add(searchResult);
        notifyItemInserted(searchResults.size() - 1);
    }

    void clear() {
        searchResults.clear();
        notifyDataSetChanged();
    }

    @Override
    @SuppressLint("InflateParams")
    public SearchResultsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SearchResultsViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_search_result, parent, false));
    }

    @Override
    public void onBindViewHolder(SearchResultsViewHolder holder, int position) {
        holder.bind(searchResults.get(position));
    }

    @Override
    public int getItemCount() {
        return searchResults.size();
    }

    class SearchResultsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.tvTitle)
        BaseTextView tvTitle;

        @BindView(R.id.tvGenre)
        BaseTextView tvGenre;

        @BindView(R.id.tvReleaseDate)
        BaseTextView tvReleaseDate;

        @BindView(R.id.tvPlot)
        BaseTextView tvPlot;

        @BindView(R.id.tvYear)
        BaseTextView tvYear;

        @BindView(R.id.ivPoster)
        ImageView ivPoster;

        void bind(SearchResult searchResult) {
            tvTitle.setText(searchResult.getTitle());
            tvGenre.setText(searchResult.getGenre());
            tvReleaseDate.setText(searchResult.getReleased());
            tvPlot.setText(searchResult.getPlot());
            tvYear.setText(searchResult.getYear());
            Glide.with(ivPoster.getContext()).load(searchResult.getPoster()).into(ivPoster);
        }

        SearchResultsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (commander != null) {
                commander.onSearchResultClicked(searchResults.get(getAdapterPosition()));
            }
        }
    }
}
