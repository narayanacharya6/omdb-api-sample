package com.narayanacharya.cubetask.search;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.narayanacharya.cubetask.R;
import com.narayanacharya.cubetask.common.base.widgets.BaseTextView;

import java.util.ArrayList;

class CategoryAdapter extends ArrayAdapter<String> {

    CategoryAdapter(Context context, ArrayList<String> users) {
        super(context, 0, users);
    }

    @Override
    @NonNull
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        // Get the data item for this position
        String category = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_search_category, parent, false);
        }
        // Lookup view for data population
        BaseTextView tvCategory = (BaseTextView) convertView.findViewById(R.id.tvSearchCategoryItem);
        tvCategory.setText(category);
        // Return the completed view to render on screen
        return convertView;
    }
}
