package com.narayanacharya.cubetask.search;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.narayanacharya.cubetask.R;
import com.narayanacharya.cubetask.common.CubeApp;
import com.narayanacharya.cubetask.common.base.widgets.BaseButton;
import com.narayanacharya.cubetask.common.base.widgets.BaseEditText;
import com.narayanacharya.cubetask.common.base.widgets.BaseTextView;
import com.narayanacharya.cubetask.data.SearchRepository;
import com.narayanacharya.cubetask.data.local.SharedPreferencesManager;
import com.narayanacharya.cubetask.data.remote.models.SearchResult;
import com.narayanacharya.cubetask.data.remote.models.SearchResults;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends Activity implements SearchContract.MovieSearchView, SearchResultsAdapter.SearchResultsClickListener {

    @BindView(R.id.tvSearchCategory)
    BaseTextView tvSearchCategory;

    @BindView(R.id.etSearch)
    BaseEditText etSearch;

    @BindView(R.id.bSearch)
    BaseButton bSearch;

    @BindView(R.id.rvSearchResults)
    RecyclerView rvSearchResults;

    @Inject
    SearchRepository searchRepository;

    private SearchPresenter searchPresenter;
    private SearchResultsAdapter searchResultsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        init();

        setupDefaultCategory();

        initializePresenter();
    }

    private void init() {
        searchResultsAdapter = new SearchResultsAdapter(new ArrayList<SearchResult>(), this);
        rvSearchResults.setAdapter(searchResultsAdapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setAutoMeasureEnabled(false);
        rvSearchResults.setLayoutManager(layoutManager);
    }

    private void initializePresenter() {
        CubeApp.getApplication().getComponent().inject(this);
        searchPresenter = new SearchPresenter(this, searchRepository);
    }

    private void setupDefaultCategory() {
        int defaultCategory = SharedPreferencesManager.getInstance(this).getDefaultSearchCategory();
        switch (defaultCategory) {
            case 0:
                tvSearchCategory.setText(getString(R.string.movies));
                break;
            case 1:
                tvSearchCategory.setText(getString(R.string.series));
                break;
            case 2:
                tvSearchCategory.setText(getString(R.string.episodes));
                break;
        }
    }

    @OnClick(R.id.searchCategoryContainer)
    void changeDefaultCategory() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);

        ArrayList<String> categories = new ArrayList<>();
        categories.add(getString(R.string.movies));
        categories.add(getString(R.string.series));
        categories.add(getString(R.string.episodes));
        CategoryAdapter adapter = new CategoryAdapter(this, categories);

        builderSingle.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SharedPreferencesManager preferencesManager = SharedPreferencesManager.getInstance(MainActivity.this);
                switch (which) {
                    case 0:
                        preferencesManager.setDefaultSearchCategory(SharedPreferencesManager.CATEGORY_MOVIE);
                        tvSearchCategory.setText(getString(R.string.movies));
                        break;
                    case 1:
                        preferencesManager.setDefaultSearchCategory(SharedPreferencesManager.CATEGORY_SERIES);
                        tvSearchCategory.setText(getString(R.string.series));
                        break;
                    case 2:
                        preferencesManager.setDefaultSearchCategory(SharedPreferencesManager.CATEGORY_EPISODE);
                        tvSearchCategory.setText(getString(R.string.episodes));
                        break;
                }
            }
        });
        builderSingle.show();
    }

    @OnClick(R.id.bSearch)
    void onSearchClicked() {
        rvSearchResults.getRecycledViewPool().clear();
        searchResultsAdapter.clear();
        String query = etSearch.getText().toString();
        String category = tvSearchCategory.getText().toString().toLowerCase();
        searchPresenter.search(query, category);
    }

    @Override
    public void onEmptySearchQuery() {
        Snackbar.make(findViewById(android.R.id.content), R.string.empty_query_error, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onSearchSuccess(SearchResults searchResults) {
        // DO NOTHING
    }

    @Override
    public void onSearchResultSuccess(SearchResult searchResult) {
        searchResultsAdapter.addSearchResult(searchResult);
    }

    @Override
    public void onSearchFailed(SearchResults searchResults) {
        if (searchResults.getError() != null)
            Snackbar.make(findViewById(android.R.id.content), searchResults.getError(), Snackbar.LENGTH_LONG).show();
        else {
            Snackbar.make(findViewById(android.R.id.content), R.string.try_again, Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void onUnknownErrorOccured() {
        Snackbar.make(findViewById(android.R.id.content), R.string.try_again, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onSearchResultClicked(SearchResult searchResult) {

    }
}
