package com.narayanacharya.cubetask.search;

import com.narayanacharya.cubetask.data.SearchRepository;
import com.narayanacharya.cubetask.data.remote.models.SearchResult;
import com.narayanacharya.cubetask.data.remote.models.SearchResults;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

class SearchPresenter {

    private SearchContract.MovieSearchView view;
    private SearchRepository searchRepository;

    SearchPresenter(SearchContract.MovieSearchView view, SearchRepository searchRepository) {
        this.view = view;
        this.searchRepository = searchRepository;
    }

    void search(String query, String category) {
        if (query.equalsIgnoreCase("")) {
            view.onEmptySearchQuery();
            return;
        }

        searchRepository
                .getSearchResults(query, category)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<SearchResults>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onUnknownErrorOccured();
                        }
                    }

                    @Override
                    public void onNext(SearchResults searchResults) {
                        if (view != null) {
                            if (searchResults.getResponse().equalsIgnoreCase("True")) {
                                view.onSearchSuccess(searchResults);
                                getDataForEachSearchResult(searchResults);
                            } else if (searchResults.getResponse().equalsIgnoreCase("False")) {
                                view.onSearchFailed(searchResults);
                            } else {
                                view.onUnknownErrorOccured();
                            }
                        }
                    }
                });
    }

    private void getDataForEachSearchResult(SearchResults searchResults) {
        for (SearchResult a : searchResults.getSearch()) {
            searchRepository
                    .getSearchResult(a.getImdbID())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Observer<SearchResult>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null) {
                                view.onUnknownErrorOccured();
                            }
                        }

                        @Override
                        public void onNext(SearchResult searchResult) {
                            if (view != null) {
                                if (searchResult.getResponse().equalsIgnoreCase("True")) {
                                    view.onSearchResultSuccess(searchResult);
                                }
                            }
                        }
                    });
        }
    }
}
