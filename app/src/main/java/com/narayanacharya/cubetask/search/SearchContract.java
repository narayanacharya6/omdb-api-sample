package com.narayanacharya.cubetask.search;

import com.narayanacharya.cubetask.data.remote.models.SearchResult;
import com.narayanacharya.cubetask.data.remote.models.SearchResults;

interface SearchContract {

    interface MovieSearchView {
        void onEmptySearchQuery();

        void onSearchSuccess(SearchResults searchResults);

        void onSearchResultSuccess(SearchResult searchResult);

        void onSearchFailed(SearchResults searchResults);

        void onUnknownErrorOccured();
    }
}
