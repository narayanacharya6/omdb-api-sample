package com.narayanacharya.cubetask.common;

import android.support.multidex.MultiDexApplication;
import android.support.v7.app.AppCompatDelegate;

import com.narayanacharya.cubetask.di.AppComponent;
import com.narayanacharya.cubetask.di.AppModule;
import com.narayanacharya.cubetask.di.DaggerAppComponent;

public class CubeApp extends MultiDexApplication {

    private AppComponent component;
    private static CubeApp application;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
        component = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public AppComponent getComponent() {
        return component;
    }

    public static CubeApp getApplication() {
        return application;
    }
}

