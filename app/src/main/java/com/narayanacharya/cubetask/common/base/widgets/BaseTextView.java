package com.narayanacharya.cubetask.common.base.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.narayanacharya.cubetask.R;
import com.narayanacharya.cubetask.util.FontCache;

public class BaseTextView extends AppCompatTextView {

    public BaseTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context, attrs);
    }

    public BaseTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        applyCustomFont(context, attrs);
    }

    private void applyCustomFont(Context context, AttributeSet attrs) {
        TypedArray attributeArray = context.obtainStyledAttributes(
                attrs,
                R.styleable.BaseFont);

        String fontName = attributeArray.getString(R.styleable.BaseFont_font);

        Typeface customFont = selectTypeface(context, fontName);
        setTypeface(customFont);

        attributeArray.recycle();
    }

    private Typeface selectTypeface(Context context, String fontName) {
        if (fontName != null) {
            if (fontName.contentEquals(context.getString(R.string.font_name_montserrat))) {
                return FontCache.getTypeface(FontCache.MONTSERRAT, context);
            }
            return null;
        } else {
            // no matching font found
            // return null so Android just uses the standard font (Roboto)
            return null;
        }
    }
}
