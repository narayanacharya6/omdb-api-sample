package com.narayanacharya.cubetask.di;

import com.narayanacharya.cubetask.search.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {

    void inject(MainActivity mainActivity);
}
