package com.narayanacharya.cubetask.data.local;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesManager {
    private SharedPreferences mSharedPreferences;
    private static final String PREFERENCES = "cube_prefs";
    private static final String DEFAULT_SEARCH_CATEGORY = "DEFAULT_SEARCH_CATEGORY";

    public static final int CATEGORY_MOVIE = 0;
    public static final int CATEGORY_SERIES = 1;
    public static final int CATEGORY_EPISODE = 2;

    private static SharedPreferencesManager sharedPreferencesManager;

    public static SharedPreferencesManager getInstance(Context context) {
        if (sharedPreferencesManager == null) {
            sharedPreferencesManager = new SharedPreferencesManager(context);
        }
        return sharedPreferencesManager;
    }

    private SharedPreferencesManager(Context context) {
        mSharedPreferences = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
    }

    public int getDefaultSearchCategory() {
        return mSharedPreferences.getInt(DEFAULT_SEARCH_CATEGORY, 0);
    }

    public void setDefaultSearchCategory(int newDefaultSearchCategory) {
        mSharedPreferences.edit().putInt(DEFAULT_SEARCH_CATEGORY, newDefaultSearchCategory).apply();
    }
}
