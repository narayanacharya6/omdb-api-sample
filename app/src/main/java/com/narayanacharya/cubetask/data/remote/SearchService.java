package com.narayanacharya.cubetask.data.remote;

import com.narayanacharya.cubetask.data.remote.models.SearchResult;
import com.narayanacharya.cubetask.data.remote.models.SearchResults;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface SearchService {

    @GET("/")
    Observable<SearchResults> getSearchResults(@Query("s") String query, @Query("category") String category);

    @GET("/")
    Observable<SearchResult> getSearchResult(@Query("i") String imdbId);

}
