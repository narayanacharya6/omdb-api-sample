package com.narayanacharya.cubetask.data;

import com.narayanacharya.cubetask.data.remote.models.SearchResult;
import com.narayanacharya.cubetask.data.remote.models.SearchResults;

import rx.Observable;

public interface SearchRepository {
    Observable<SearchResults> getSearchResults(String query, String category);

    Observable<SearchResult> getSearchResult(String imdbId);
}
