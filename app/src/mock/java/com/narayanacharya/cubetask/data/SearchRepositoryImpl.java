package com.narayanacharya.cubetask.data;

import android.content.Context;

import com.narayanacharya.cubetask.data.remote.models.SearchResult;
import com.narayanacharya.cubetask.data.remote.models.SearchResults;
import com.narayanacharya.cubetask.utils.TestUtils;

import rx.Observable;

public class SearchRepositoryImpl implements SearchRepository {

    private Context context;

    public SearchRepositoryImpl(Context context) {
        this.context = context;
    }

    @Override
    public Observable<SearchResults> getSearchResults(String s, String category) {
        return Observable.just(TestUtils.responseObject(context, "movie_search_results.json",
                SearchResults.class));
    }

    @Override
    public Observable<SearchResult> getSearchResult(String imdbId) {
        return Observable.just(TestUtils.responseObject(context, "movie_search_result.json",
                SearchResult.class));
    }
}
