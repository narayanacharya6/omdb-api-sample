package com.narayanacharya.cubetask.utils;

import android.content.Context;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Naru on 1/2/2017.
 * <p>
 * Utility class for helper methods needed while testing.
 */

public class TestUtils {

    private static String readFileFromAssets(String fileName, Context c) {
        try {
            InputStream is = c.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            int n = is.read(buffer);
            is.close();
            return new String(buffer);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T responseObject(Context context, String fileName, Class<T> objectClass) {
        return new Gson().fromJson(readFileFromAssets(fileName, context), objectClass);
    }
}
