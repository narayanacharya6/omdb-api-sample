package com.narayanacharya.cubetask.di;

import android.app.Application;

import com.narayanacharya.cubetask.data.SearchRepository;
import com.narayanacharya.cubetask.data.SearchRepositoryImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    SearchRepository provideSearchRepository() {
        return new SearchRepositoryImpl(application);
    }
}

